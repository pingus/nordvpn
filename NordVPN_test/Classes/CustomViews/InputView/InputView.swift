//
//  InputView.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 08.05.2021.
//

import UIKit

typealias TextDidChange = (String?) -> Void?

@objc enum InputType: Int {
    case username
    case password
}

// MARK: Custom view for input username or password

@IBDesignable
final class InputView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    // MARK: Outlets
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var visibleButton: UIButton!
    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var textField: UITextField!
    var textDidChange: TextDidChange?
    
    // MARK: Parameters can be configured
    @IBInspectable
    var contentBackgroundColor: UIColor = .colorGray1 {
        didSet {
            updateUI()
        }
    }
    @IBInspectable
    var contentViewRadius: CGFloat = 10.0 {
        didSet {
            updateUI()
        }
    }
    @IBInspectable
    var inputType: InputType = .username {
        didSet {
            updateUI()
        }
    }

    // MARK: Initializers
    init(frame: CGRect, viewType: InputType) {
        super.init(frame: frame)

        xibSetup()
        inputType = viewType
        updateUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
 
        xibSetup()
        updateUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        xibSetup()
        updateUI()
    }
        
    private func updateUI() {
        contentView.layer.cornerRadius = contentViewRadius
        contentView.backgroundColor = contentBackgroundColor
        setupTextField()
        setupIcons(state: .inactive)
        textField.delegate = self
    }
    
    private func setupTextField() {
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        switch inputType {
        case .username:
            textField.placeholder = "username".localized
            textField.isSecureTextEntry = false
        case .password:
            textField.placeholder = "password".localized
            textField.isSecureTextEntry = true
        }
    }
    
    private func setupIcons(state: UIImage.IconState) {
        switch inputType {
        case .username:
            iconView.image = .icon(iconType: .username, iconState: state)
            visibleButton.isHidden = true
        case .password:
            iconView.image = .icon(iconType: .password, iconState: state)
            visibleButton.isHidden = false
            visibleButton.setImage(.icon(iconType: .iconInvisible, iconState: state)?.resized(to: CGSize(width: Constants.visibleIconSize, height: Constants.visibleIconSize)), for: .normal)
        }
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // MARK: Show/hide password Action
    
    @IBAction func showHideAction(_ sender: Any) {
        if textField.isSecureTextEntry {
            visibleButton.setImage(.icon(iconType: .iconVisible, iconState: .active)?.resized(to: CGSize(width: Constants.visibleIconSize, height: Constants.visibleIconSize)), for: .normal)
            textField.isSecureTextEntry = false
        } else {
            visibleButton.setImage(.icon(iconType: .iconInvisible, iconState: .active)?.resized(to: CGSize(width: Constants.visibleIconSize, height: Constants.visibleIconSize)), for: .normal)
            textField.isSecureTextEntry = true
        }
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of:self))
        let nib = UINib(nibName: InputView.className, bundle: bundle)
        
        // Assumes UIView is top level and only object in InputView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func getText() -> String? {
        return textField.text
    }
}

// MARK: Constants
extension InputView {
    
    private enum Constants {
        static let visibleIconSize: CGFloat = 18.0
    }
}

extension InputView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setupIcons(state: .active)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        setupIcons(state: .inactive)
        if inputType == .password {
            textField.isSecureTextEntry = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate: String? = nil
        if let text = textField.text as NSString? {
            txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            textField.text = txtAfterUpdate
        }
        textDidChange?(txtAfterUpdate)
        return false
    }
    
}
