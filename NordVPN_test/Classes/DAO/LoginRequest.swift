//
//  LoginRequest.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 12.05.2021.
//

import Foundation

typealias CompletionHandler = (_ success:Bool, _ error: Error?) -> Void

class LoginRequest: NSObject {
    
    static func loginUser(with username: String, password: String, completion: CompletionHandler?) {
        var paramsDictionary = [String: Any]()
        paramsDictionary["username"] = username
        paramsDictionary["password"] = password
        
        HttpClientApi().makeAPICall(url: Constants.loginURL, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            guard let data = data, let response = response else {
                completion?(false, nil)
                return
            }
            let session = try? AuthModel.initFromJSONData(DataResponse(data: data, response: response))
            session?.save(then:  { (result) in
                completion?(true, nil)
            })
            // API call is Successfull
            
        }, failure: { (data, response, error) in
            if let error = error {
                completion?(false, error)
                return
            }
            if response?.statusCode == NSError.Codes.unauthorized {
                completion?(false, NSError(domain: NSError.Constants.domain, code: NSError.Codes.unauthorized, userInfo: NSError.Constants.loginError))
                return
            }
           
            completion?(true, nil)
        })
    }
}

extension LoginRequest {
    private enum Constants {
        static let loginURL: String = "https://playground.tesonet.lt/v1/tokens"
    }
}
