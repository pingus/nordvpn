//
//  AuthModel.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import Foundation
import RealmSwift
 
final class AuthModel: StoredObject, Codable {
    
    @objc dynamic var primeKey: String = SingletonRecordID // artificially added to make model singleton-like, and support fetch by primary key
    @objc dynamic var token: String = ""
    
    enum CodingKeys: String, CodingKey {
        case primeKey, token
    }
    
    override class func primaryKey() -> String? {
        return CodingKeys.primeKey.rawValue
    }
    
    override class var updatePolicy: RealmUpdatePolicy {
        return .modified
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        primeKey = SingletonRecordID
        token = try container.decode(String.self, forKey: .token)
    }
    
    required init() {
        super.init()
    }
}
