//
//  BackgroundThread.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import Foundation

public final class RunLoopSource {
 
    private lazy var source: CFRunLoopSource = {
        var context = CFRunLoopSourceContext()
        context.info = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        context.perform = _perform

        return CFRunLoopSourceCreate(nil, self.order, &context)
    }()

    private let subscription: (CFRunLoop, CFRunLoopMode)
    private let order: CFIndex
    fileprivate var perform: ()->Void
 
    public init(loop: CFRunLoop = CFRunLoopGetCurrent(), mode: CFRunLoopMode = CFRunLoopMode.commonModes, order: CFIndex = 0, perform: @escaping ()->Void) {
        self.subscription = (loop, mode)
        self.perform = { perform() }
        self.order = order
        CFRunLoopAddSource(loop, source, mode)
    }

    deinit {
        let (loop, mode) = subscription
        CFRunLoopRemoveSource(loop, source, mode)
    }
 
    public func signal() {
        CFRunLoopSourceSignal(source)
    }
}

fileprivate func _perform(info: UnsafeMutableRawPointer?) {
    unsafeBitCast(info!, to: RunLoopSource.self).perform()
}
 
public class BackgroundThread: Thread {
 
    public let label: String
    private var runloop: RunLoop!
    private var source: RunLoopSource!
    
    override public var name: String? {
        get { return label }
        set {}
    }
    
    public init(label: String) {
        self.label = label
        super.init()
    }
    
    deinit {
        source = nil
    }

    public override func main() {
        runloop = RunLoop.current
        source = RunLoopSource(loop: runloop.getCFRunLoop(), perform: { [weak self] in
            self?.cancel()
        })
        while (!self.isCancelled) {
            RunLoop.current.run( mode: .default, before: .distantFuture)
        }
        source = nil
        Thread.exit()
    }
    
    public func stop() {
        source.signal()
        cancel()
    }
}

extension BackgroundThread {
    
    @objc public func async(_ task: @convention(block) @escaping ()->Void) {
        self.perform(#selector(self.do(block:)), on: self, with: task, waitUntilDone: false, modes: [RunLoop.Mode.default.rawValue])
    }
    
    @objc private func `do`(block: @convention(block) @escaping ()->Void) {
        block()
    }
}
