//
//  ServerModel.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 18.05.2021.
//

import Foundation
import RealmSwift
 
final class ServerModel: StoredObject, Codable {
    
    @objc dynamic var name: String = ""
    @objc dynamic var distance: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case name, distance
    }
    
    override class func primaryKey() -> String? {
        return CodingKeys.distance.rawValue
    }
    
    override class var updatePolicy: RealmUpdatePolicy {
        return .modified
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        distance = try container.decode(Int.self, forKey: .distance)
    }
    
    required init() {
        super.init()
    }
}
