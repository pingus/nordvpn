//
//  StorageObject.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import Foundation
import RealmSwift

public let SingletonRecordID = "SingletonRecordID"
public let SingletonServerRecordID = "SingletonServerRecordID"


/// Base class to work with realm models, provides some default functionality
/// Subclass it!
///
/// https://realm.io/docs/swift/latest/#property-cheatsheet
open class StoredObject: Object, BaseStoredObject {
    
    @objc dynamic var __unused = 0 // Throw an exception for Objects that have none of its properties marked with @objc. https://github.com/realm/realm-cocoa/releases/tag/v10.1.0
     
    required public override init() {
        super.init()
    }
 
    /// Override to set the model’s primary key. Declaring a primary key allows objects to be looked up and updated efficiently and enforces uniqueness for each value. Once an object with a primary key is added to a Realm, the primary key cannot be changed.
    ///
    /// https://realm.io/docs/swift/latest/#primary-keys
    override open class func primaryKey() -> String? {
        return nil
    }
 
    /// Override to index a property. Like primary keys, indexes make writes slightly slower, but makes queries using equality and IN operators faster. (It also makes your Realm file slightly larger, to store the index.) It’s best to only add indexes when you’re optimizing the read performance for specific situations.
    ///
    /// https://realm.io/docs/swift/latest/#indexing-properties
    override open class func indexedProperties() -> [String] {
        return []
    }
    
    /// Override if you don’t want to save a field in your model to its Realm. Realm won’t interfere with the regular operation of these properties; they’ll be backed by ivars, and you can freely override their setters and getters.
    ///
    /// https://realm.io/docs/swift/latest/#ignoring-properties
    override open class func ignoredProperties() -> [String] {
        return []
    }
    
    /// When updating objects you can choose to either have all of the properties on the existing object set to the passed-in value, or only the properties which have actually changed to new values.
    /// You may not pass update: `.modified` or update: `.all` for object types which don’t define a primary key.
    /// When in doubt, .modified is probably the one you want.
    ///
    /// https://realm.io/docs/swift/latest/#updating-objects
    open class var updatePolicy: RealmUpdatePolicy {
        return primaryKey() == nil ? .error : .modified
    }
    
}
