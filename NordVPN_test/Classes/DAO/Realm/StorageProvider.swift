//
//  StorageProvider.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import Foundation
import RealmSwift


public struct DataResponse {
    let data: Data
    let response: URLResponse
    
    init(data: Data?, response: URLResponse) {
        self.data = data ?? Data()
        self.response = response
    }
}


public typealias OnCompletion = () -> Void
public typealias OnVoidResult = (Result<Void, Error>) -> Void
public typealias OnDataResult = (Result<DataResponse, Error>) -> Void

public typealias OnSomeResult<T: Any> = (Result<T, Error>) -> Void
public typealias OnSomeOptionalResult<T: Any> = (Result<T?, Error>) -> Void
 
extension Result where Success: Any, Failure: Error {
    
    var voidResult: Result<Void, Failure> {
        map { _ in return () }
    }
}


public typealias RealmConfiguration = Realm.Configuration
public typealias RealmUpdatePolicy = Realm.UpdatePolicy
public typealias RealmNotificationToken = NotificationToken

public typealias OnEditObject<T: ThreadConfined> = (T?) throws -> Void
public typealias OnEditResults<T: RealmCollectionValue & ThreadConfined> = (Results<T>?) throws -> Void
public typealias OnEditList<T: RealmCollectionValue & ThreadConfined> = (List<T>?) throws -> Void
public typealias OnOptionalWrappedObjectResult<T: ThreadConfined> = OnSomeOptionalResult<SafeThreadConfined<T>>

public enum SafeThreadConfined<T: ThreadConfined> {
    
    case managed(threadSafe: ThreadSafeReference<T>)
    case unmanaged(object: T)
    
    private init(object: T) {
        if (object as ThreadConfined).realm != nil {
            let ts = ThreadSafeReference(to:object)
            self = .managed(threadSafe: ts)
        } else {
            self = .unmanaged(object: object)
        }
    }
    
    static func with(object: T) -> Self {
        return Self.self.init(object: object)
    }
    
    func resolved(in realm: Realm) -> T? {
        switch self {
        case .managed(let threadSafe):
            let object = realm.resolve(threadSafe)
            return object
        case .unmanaged(let object):
            return object
        }
    }
}

extension ThreadConfined {
    
    var threadSafe: SafeThreadConfined<Self> {
        return SafeThreadConfined.with(object: self)
    }
}
 
public protocol BaseStoredObject: Object {
    
    static var updatePolicy: RealmUpdatePolicy { get }
}

protocol DetachableObject: AnyObject {
    
    func detached() -> Self
}

public protocol StorageProvider: AnyObject {
    
    func fetch<T: BaseStoredObject>(single object: T.Type, forPrimaryKey key: String) throws -> T?
    func fetch<T: BaseStoredObject>(all objects: T.Type) throws -> Results<T>
    
    func save<T: BaseStoredObject>(_ object: T, edit: OnEditObject<T>?, then onResult: OnOptionalWrappedObjectResult<T>?)
    func save<T: BaseStoredObject>(_ objects: List<T>, edit: OnEditList<T>?, then onResult: OnOptionalWrappedObjectResult<List<T>>?)
    func save<T: BaseStoredObject>(_ objects: Results<T>, edit: OnEditResults<T>?, then onResult: OnOptionalWrappedObjectResult<Results<T>>?)
 
    func delete<T: BaseStoredObject>(_ object: T, then onResult: OnVoidResult?)
    func delete<T: BaseStoredObject>(_ objects: List<T>, then onResult: OnVoidResult?)
    func delete<T: BaseStoredObject>(_ objects: Results<T>, then onResult: OnVoidResult?)
}
 
class RealmProvider: StorageProvider {
 
    static let realmFile = "test.storage"
    static let schemaVersion: UInt64 = 1
    
    fileprivate let notificationThread = BackgroundThread(label: "RealmProvider notification listener")
    public let configuration: RealmConfiguration
    private let queue: DispatchQueue
    
    public func realm() throws -> Realm {
        
            ///  Realm instances are cached internally, and constructing equivalent Realm objects (for example, by using the same path or identifier) produces limited overhead.
            ///  If you specifically want to ensure a Realm instance is destroyed (for example, if you wish to open a Realm, check some property, and then possibly delete the Realm file and re-open it), place the code which uses the Realm within an autoreleasepool {} and ensure you have no other strong references to it.
            ///
            ///  https://realm.io/docs/swift/4.1.0/api/Structs/Realm.html
            
            // regarding to above comment - creating realm instances produce limited overhead
            // and for write transacrions - we should use autorelease pool to limit the relam file growing
        
        
        do {
            let realm = try Realm(configuration: configuration)
            return realm
        } catch {
            
            NotificationCenter.default.post(name: .logout, object: nil)
            
            if let url = configuration.fileURL, FileManager.default.fileExists(atPath: url.path) {
                let realmURLs = [
                    url,
                    url.appendingPathExtension("lock"),
                    url.appendingPathExtension("note"),
                    url.appendingPathExtension("management"),
                ]
                for URL in realmURLs {
                    try? FileManager.default.removeItem(at: URL)
                }
            }
            do {
                let realm = try Realm(configuration: configuration)
                return realm
            } catch {
                print(error, logLevel: .ERROR)
                throw error
            }
        }
    }
 
    /// returns default realm provider, also overrides default realm configuration
    static let `default`: RealmProvider = {
        RealmConfiguration.defaultConfiguration = RealmProvider.defaultRealmConfiguration
        return RealmProvider(configuration: RealmProvider.defaultRealmConfiguration)
    }()
    
    init(configuration: RealmConfiguration) {
        self.configuration = configuration
        notificationThread.start()
        self.queue = DispatchQueue.init(label: "\(String(describing: RealmProvider.self)) \(configuration.fileURL?.absoluteString ?? ""))", attributes: .concurrent)
    }
    
    deinit {
        notificationThread.stop()
    }

    public static var defaultRealmConfiguration: RealmConfiguration {
        let file = FileManager.default.documentsDirUrl!.appendingPathComponent(RealmProvider.realmFile)
        
        let config = Realm.Configuration.init(fileURL: file,
                                              encryptionKey: getEncryptionKey() as Data,
                                              readOnly: false,
                                              schemaVersion: RealmProvider.schemaVersion,
                                              migrationBlock: { migration, oldSchemaVersion in
                                                if oldSchemaVersion < RealmProvider.schemaVersion {
                                                    print("Migration started 1️⃣ 🁢🁢🁢🁢🁢🁢🁢🁢🁢🁣🁣🁣🁣🁣🁣🁣 ", logLevel: .INFO)
                                                }
                                                print("Migration complete 🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢🁢 🌈", logLevel: .INFO)
                                              },
                                              deleteRealmIfMigrationNeeded: false,
                                              shouldCompactOnLaunch: { (_,_) in return true })
        return config
    }
    
    func notifyOnUpdate<T: BaseStoredObject>(for object: T, notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        let threadSafe = object.threadSafe
        notificationThread.async { [weak self] in
            if let self = self, let resolved = try? threadSafe.resolved(in: self.realm()) {
                let token = resolved.observe { (result) in
                    switch result {
                    case .change(_, _), .deleted:
                        DispatchQueue.main.async {
                            notify()
                        }
                    default: break;
                    }
                }
                result(token)
            } else {
                result(nil)
            }
        }
    }
    
    func notifyOnUpdate<T: BaseStoredObject>(for type: T.Type, notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        notificationThread.async {
            let token = try? type.fetch().observe { (result) in
                if case .update = result {
                    DispatchQueue.main.async {
                        notify()
                    }
                }
            }
            result(token)
        }
    }
    
    func notifyOnUpdate<T: BaseStoredObject>(for results: Results<T>, notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        let threadSafe = results.threadSafe
        notificationThread.async { [weak self] in
            if let self = self, let resolved = try? threadSafe.resolved(in: self.realm()) {
                let token = resolved.observe { (result) in
                    if case .update = result {
                        DispatchQueue.main.async {
                            notify()
                        }
                    }
                }
                result(token)
            } else {
                result(nil)
            }
        }
    }
    
    func notifyOnUpdate<T: BaseStoredObject>(for list: List<T>, notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        let threadSafe = list.threadSafe
        notificationThread.async { [weak self] in
            if let self = self, let resolved = try? threadSafe.resolved(in: self.realm()) {
                let token = resolved.observe { (result) in
                    if case .update = result {
                        DispatchQueue.main.async {
                            notify()
                        }
                    }
                }
                result(token)
            } else {
                result(nil)
            }
        }
    }
    
    public func writeAsync(_ block: @escaping ((Realm) throws -> Void), then onResult: OnVoidResult? = nil) {
        queue.async(flags: .barrier) {
            autoreleasepool {
                do {
                    let realm = try self.realm()
                    try realm.write {
                        try block(realm)
                    }
                    DispatchQueue.main.async { [weak self] in
                        // force refresh realm on thread which will receive result, as it might not receive the update yet
                        // performance impact should be negligible
                        _ = try? self?.realm().refresh()
                        onResult?(.success(()))
                    }
                }
                catch {
                    DispatchQueue.main.async { [weak self] in
                        // force refresh realm on thread which will receive result, as it might not receive the update yet
                        // performance impact should be negligible
                        _ = try? self?.realm().refresh()
                        onResult?(.failure(error))
                    }
                }
            }
        }
    }
 
    public func writeAsync<T: ThreadConfined>(obj: T, block: @escaping ((Realm, T?) throws -> Void), then onResult: OnOptionalWrappedObjectResult<T>? = nil) {
        let wrapped = obj.threadSafe
        var editedWrapped: SafeThreadConfined<T>? = nil
        writeAsync({ (realm) in
            let edited = wrapped.resolved(in: realm)
            try block(realm,edited)
            editedWrapped = edited?.threadSafe
        }, then: { (result) in
            switch result {
            case .success:
                onResult?(.success(editedWrapped))
            case .failure(let error):
                onResult?(.failure(error))
            }
        })
    }
    
    func fetch<T: BaseStoredObject>(single object: T.Type, forPrimaryKey key: String) throws -> T? {
        let realm = try self.realm()
        let result = realm.object(ofType: object, forPrimaryKey: key)
        
        return result
    }

    func fetch<T: BaseStoredObject>(all objects: T.Type) throws -> Results<T> {
        let realm = try self.realm()
        let result = realm.objects(objects)

        return result
    }
    
    func save<T: BaseStoredObject>(_ object: T, edit: OnEditObject<T>? = nil, then onResult: OnOptionalWrappedObjectResult<T>? = nil) {
        writeAsync(obj: object, block: { (realm, object) in
            if let object = object {
                try edit?(object)
                realm.create(T.self, value: object, update: T.self.updatePolicy)
            }
        }, then: onResult)
    }
 
    func save<T: BaseStoredObject>(_ objects: List<T>, edit: OnEditList<T>? = nil, then onResult: OnOptionalWrappedObjectResult<List<T>>? = nil) {
        writeAsync(obj: objects, block: { (realm, objects) in
            if let objects = objects {
                try edit?(objects)
                for object in objects {
                    realm.create(T.self, value: object, update: T.self.updatePolicy)
                }
            }
        }, then: onResult)
    }
    
    func save<T: BaseStoredObject>(_ objects: Results<T>, edit: OnEditResults<T>? = nil, then onResult: OnOptionalWrappedObjectResult<Results<T>>? = nil) {
        writeAsync(obj: objects, block: { (realm, objects) in
            if let objects = objects {
                try edit?(objects)
                for object in objects {
                    realm.create(T.self, value: object, update: T.self.updatePolicy)
                }
            }
        }, then: onResult)
    }
 
    func delete<T: BaseStoredObject>(_ object: T, then onResult: OnVoidResult? = nil) {
        writeAsync(obj: object, block: { (realm, object) in
            if let object = object {
                realm.delete(object)
            }
        }, then: { (result) in
            switch result {
            case .success:
                onResult?(.success(()))
            case .failure(let error):
                onResult?(.failure(error))
            }
        })
    }
    
    func delete<T: BaseStoredObject>(_ objects: List<T>, then onResult: OnVoidResult? = nil) {
        writeAsync(obj: objects.self, block: { (realm, objects) in
            if let objects = objects {
                realm.delete(objects)
            }
        }, then: { (result) in
            switch result {
            case .success:
                onResult?(.success(()))
            case .failure(let error):
                onResult?(.failure(error))
            }
        })
    }
    
    func delete<T: BaseStoredObject>(_ objects: Results<T>, then onResult: OnVoidResult? = nil) {
        writeAsync(obj: objects.self, block: { (realm, objects) in
            if let objects = objects {
                realm.delete(objects)
            }
        }, then: { (result) in
            switch result {
            case .success:
                onResult?(.success(()))
            case .failure(let error):
                onResult?(.failure(error))
            }
        })
    }
    
    private static func getEncryptionKey() -> NSData {
        // Identifier for our keychain entry
        let keychainIdentifier = "group.\(Bundle.main.bundleIdentifier ?? "")"
        let keychainIdentifierData = keychainIdentifier.data(using: String.Encoding.utf8, allowLossyConversion: false)!
            
        // First check in the keychain for an existing key
        var query: [NSString: AnyObject] = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
            kSecAttrKeySizeInBits: 512 as AnyObject,
            kSecReturnData: true as AnyObject
        ]
            
        var dataTypeRef: AnyObject?
        var status = withUnsafeMutablePointer(to: &dataTypeRef) { SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0)) }
        if status == errSecSuccess {
            return dataTypeRef as! NSData
        }
            
        // No pre-existing key, so generate a new one
        let keyData = NSMutableData(length: 64)!
        let result = SecRandomCopyBytes(kSecRandomDefault, 64, keyData.mutableBytes.bindMemory(to: UInt8.self, capacity: 64))
        assert(result == 0, "Failed to get random bytes")
            
        // Store the key in the keychain
        query = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
            kSecAttrKeySizeInBits: 512 as AnyObject,
            kSecValueData: keyData
        ]
            
        status = SecItemAdd(query as CFDictionary, nil)
        assert(status == errSecSuccess, "Failed to insert the new key in the keychain")
            
        return keyData
    }
}
 
extension BaseStoredObject {

    static func fetch(forPrimaryKey key: String) throws -> Self? {
        let result = try RealmProvider.default.fetch(single: self.self, forPrimaryKey: key)
        
        return result
    }
    
    static func fetch() throws -> Results<Self> {
        let result = try RealmProvider.default.fetch(all: self.self)
        
        return result
    }
    
    static func fetch(`where` predicateFormat: String, _ args: Any...) throws -> Results<Self> {
        let result = try RealmProvider.default.fetch(all: self.self).filter(predicateFormat, args)
        
        return result
    }

    func save(edit: OnEditObject<Self>? = nil, then onResult: OnOptionalWrappedObjectResult<Self>? = nil) {
        RealmProvider.default.save(self, edit: edit, then: onResult)
    }

    func delete(then onResult: OnVoidResult? = nil) {
        RealmProvider.default.delete(self, then: onResult)
    }
    
    func notifyOnUpdate(_ notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        RealmProvider.default.notifyOnUpdate(for: self, notify: notify, result: result)
    }
    
    static func notifyOnUpdate(_ notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        RealmProvider.default.notifyOnUpdate(for: Self.self, notify: notify, result: result)
    }
}
 
extension Results where ElementType: BaseStoredObject {
    
    func save(edit: OnEditResults<ElementType>? = nil, then onResult: OnOptionalWrappedObjectResult<Results<Element>>? = nil) {
        RealmProvider.default.save(self, edit: edit, then: onResult)
    }
    
    func delete(then onResult: OnVoidResult? = nil) {
        RealmProvider.default.delete(self, then: onResult)
    }
    
    func detached() -> [Element] {
        return self.map{ $0.detached()}
    }
    
    func notifyOnUpdate(notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        RealmProvider.default.notifyOnUpdate(for: self, notify: notify, result: result)
    }
}

extension List: DetachableObject where ElementType: BaseStoredObject {
    
    func save(edit: OnEditList<ElementType>? = nil, then onResult: OnOptionalWrappedObjectResult<List<Element>>? = nil) {
        RealmProvider.default.save(self, edit: edit, then: onResult)
    }
    
    func delete(then onResult: OnVoidResult? = nil) {
        RealmProvider.default.delete(self, then: onResult)
    }
    
    func detached() -> Self {
        let result = Self()
        result.append(objectsIn: self.map{ $0.detached()})
        return result
    }
    
    func notifyOnUpdate(notify: @escaping ()->Void, result: @escaping (Any?)->Void) {
        RealmProvider.default.notifyOnUpdate(for: self, notify: notify, result: result)
    }
}
 
extension Object: DetachableObject {
    
    func detached() -> Self {
        let detached = type(of: self).init()
        for property in objectSchema.properties {
            guard let value = value(forKey: property.name) else { continue }
            if property.isArray, let detachable = value as? DetachableObject {
                //Realm List
                detached.setValue(detachable.detached(), forKey: property.name)
            } else if property.type == .object, let detachable = value as? DetachableObject {
                //Realm Object
                detached.setValue(detachable.detached(), forKey: property.name)
            } else {
                detached.setValue(value, forKey: property.name)
            }
        }
        return detached
    }
}

extension UUID {
    //    8      4    4    4      12
    // 09d85e01-479f-496f-8fc2-210b6b4c7fe3
    var backendFormattedGUIDString : String {
        let str = self.uuidString
        let groups = [8,4,4,4,12]
        var values = [String]()
        for (group) in groups {
            values.append(String(str.prefix(group)))
        }
        return values.joined(separator: "-")
    }
}

@objc enum CRUD: Int, RealmEnum {
    case create
    case read
    case update
    case delete
}
