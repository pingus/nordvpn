//
//  HttpApiClient.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 12.05.2021.
//

import Foundation

//HTTP Methods
enum HttpMethod : String {
   case  GET
   case  POST
   case  DELETE
   case  PUT
}


class HttpClientApi: NSObject{

    private var request : URLRequest?
    private var session : URLSession?

    func makeAPICall(url: String,params: Dictionary<String, Any>?, method: HttpMethod, token: String? = nil, success:@escaping ( Data? ,HTTPURLResponse?  , NSError? ) -> Void, failure: @escaping ( Data? ,HTTPURLResponse?  , NSError? )-> Void) {

        request = URLRequest(url: URL(string: url)!)
        
        print("URL = \(url)", logLevel: .DEBUG)
        
        if let params = params {
            let  jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request?.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request?.addValue("application/json", forHTTPHeaderField: "Accept")

            request?.httpBody = jsonData
        }
        if let token = token {
            request?.setValue( "Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        request?.httpMethod = method.rawValue
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Constants.timeoutInterval
        configuration.timeoutIntervalForResource = Constants.timeoutInterval
        
        session = URLSession(configuration: configuration)
        session?.dataTask(with: request! as URLRequest) { (data, response, error) -> Void in
            if let data = data {
                if let response = response as? HTTPURLResponse, Constants.successResponseCodes ~= response.statusCode {
                    success(data , response , error as NSError?)
                } else {
                    failure(data , response as? HTTPURLResponse, error as NSError?)
                }
            }else {
                failure(data , response as? HTTPURLResponse, error as NSError?)
            }
        }.resume()
    }
}

extension HttpClientApi {
    
    private enum Constants {
        static let successResponseCodes = 200...299
        static let timeoutInterval: TimeInterval = 30
    }
}
