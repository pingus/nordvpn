//
//  ServerListRequest.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 18.05.2021.
//

import Foundation

class ServerListRequest: NSObject {
    
    static func getServerList(completion: CompletionHandler?) {
        
        guard let auth = try? AuthModel.fetch(forPrimaryKey: SingletonRecordID)?.detached(), auth.token.count > 0 else {
            NotificationCenter.default.post(name: .logout, object: nil)
            return
        }
        
        HttpClientApi().makeAPICall(url: Constants.serverListURL, params: nil, method: .GET, token: auth.token, success: { (data, response, error) in
            guard let data = data, let response = response else {
                DispatchQueue.main.async {
                    completion?(false, nil)
                }
                return
            }
            let servers = try? ServerModel.initFromJSONArrayData(DataResponse(data: data, response: response))
            servers?.forEach({ server in
                server.save()
            })
            DispatchQueue.main.async {
                completion?(true, nil)
            }
        }, failure: { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    completion?(false, error)
                }
                return
            }
            if response?.statusCode == NSError.Codes.unauthorized {
                DispatchQueue.main.async {
                    completion?(false, NSError(domain: NSError.Constants.domain, code: NSError.Codes.unauthorized, userInfo: NSError.Constants.loginError))
                }
                return
            }
            DispatchQueue.main.async {
                completion?(false, nil)
            }
        })
    }
}

extension ServerListRequest {
    private enum Constants {
        static let serverListURL: String = "https://playground.tesonet.lt/v1/servers"
    }
}
