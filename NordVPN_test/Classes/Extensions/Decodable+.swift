//
//  Decodable+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import Foundation

public var defaultEncoder: JSONEncoder {
    let encoder = JSONEncoder()
    
    let formatter = Date.backendFormatter
    
    encoder.dateEncodingStrategy = .formatted(formatter)
    
    return encoder
}

public var defaultDecoder: JSONDecoder {
    let encoder = JSONDecoder()
    
    let formatter = Date.backendFormatter
    
    encoder.dateDecodingStrategy = .formatted(formatter)
    
    return encoder
}



extension Decodable {
    
    static func initFromJSONData(data: Data, decoder: JSONDecoder = defaultDecoder) throws -> Self {
        do {
            let obj = try decoder.decode(Self.self, from: data)
            return obj
        } catch {
            print(error, logLevel: .ERROR)
            throw error
        }
    }

    static func initFromJSONArrayData(data: Data, decoder: JSONDecoder = defaultDecoder) throws -> [Self] {
        do {
            let obj = try decoder.decode([Self].self, from: data)
            return obj
        } catch {
            print(error, logLevel: .ERROR)
            throw error
        }
    }

    static func initFromJSONDataOptional(data: Data?, decoder: JSONDecoder = defaultDecoder) -> Self? {
        if let data = data {
            do {
                let obj = try decoder.decode(Self.self, from: data)
                return obj
            } catch {
                //print(error, logLevel: .ERROR)
                return nil
            }
        }
        return nil
    }
    
    static func unwrapData(data: Data, onResult: @escaping OnSomeResult<Self>) {
        do {
            let obj = try Self.initFromJSONData(data: data)
            onResult(.success(obj))
        } catch {
            onResult(.failure(error))
        }
    }

    static func unwrapArrayData(data: Data, onResult: @escaping OnSomeResult<[Self]>) {
        do {
            let obj = try Self.initFromJSONArrayData(data: data)
            onResult(.success(obj))
        } catch {
            onResult(.failure(error))
        }
    }

    init(from: Any) throws {
        do {
            let data = try JSONSerialization.data(withJSONObject: from, options: .prettyPrinted)
            let decoder = defaultDecoder
            self = try decoder.decode(Self.self, from: data)
        } catch {
            print(error, logLevel: .ERROR)
            throw error
        }
    }
    
    static func initFromJSONData(_ data: DataResponse, decoder: JSONDecoder = defaultDecoder) throws -> Self {
        decoder.userInfo[CodingUserInfoKey.urlResponse] = data.response
        return try initFromJSONData(data: data.data, decoder: decoder)
    }
    
    static func initFromJSONArrayData(_ data: DataResponse, decoder: JSONDecoder = defaultDecoder) throws -> [Self] {
        decoder.userInfo[CodingUserInfoKey.urlResponse] = data.response
        return try initFromJSONArrayData(data: data.data, decoder: decoder)
    }
    
    static func initFromJSONDataOptional(_ data: DataResponse, decoder: JSONDecoder = defaultDecoder) -> Self? {
        decoder.userInfo[CodingUserInfoKey.urlResponse] = data.response
        return initFromJSONDataOptional(data: data.data, decoder: decoder)
    }
    
    static func unwrapData(_ data: DataResponse, onResult: @escaping OnSomeResult<Self>) {
        do {
            let obj = try Self.initFromJSONData(data)
            onResult(.success(obj))
        } catch {
            onResult(.failure(error))
        }

    }

    static func unwrapArrayData(_ data: DataResponse, onResult: @escaping OnSomeResult<[Self]>) {
        do {
            let obj = try Self.initFromJSONArrayData(data)
            onResult(.success(obj))
        } catch {
            onResult(.failure(error))
        }
    }
}

extension CodingUserInfoKey {
   static let urlResponse = CodingUserInfoKey(rawValue: "urlResponse")!
}
