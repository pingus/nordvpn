//
//  FileManager+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import Foundation

extension FileManager {
    
    var documentsDirUrl: URL? {
        return  urls(for: .documentDirectory, in: .userDomainMask).first
    }
}
