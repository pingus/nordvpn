//
//  Font+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import UIKit

extension UIFont {
    
    static var titleFont = UIFont(name: FontName.sfPro, size: Size.size17) ?? UIFont.systemFont(ofSize: Size.size17)
    static var descriptionFont = UIFont(name: FontName.sfPro, size: Size.size13) ?? UIFont.systemFont(ofSize: Size.size13)
    static var navTitleFont = UIFont(name: FontName.sfProSemibold, size: Size.size17) ?? UIFont.systemFont(ofSize: Size.size17, weight: UIFont.Weight.medium)
    static var headerTitleFont = UIFont(name: FontName.sfPro, size: Size.size12) ?? UIFont.systemFont(ofSize: Size.size12, weight: UIFont.Weight.medium)
}

extension UIFont {
    
    private enum FontName {
        static let sfPro = "SF Pro Text"
        static let sfProSemibold = "SF Pro Text Semibold"
    }
    private enum Size {
        static let size17: CGFloat = 17
        static let size13: CGFloat = 13
        static let size12: CGFloat = 12
    }
}
