//
//  NSError+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 12.05.2021.
//

import Foundation

extension NSError {
    
    enum Constants {
        static let domain = "nordVPN_Test"
        static let loginError: [String: Any] = [NSLocalizedDescriptionKey : "login_error".localized]
        static let serverError: [String: Any] = [NSLocalizedDescriptionKey : "server_error".localized]
    }
    
    enum Codes {
        static let unauthorized = 400
        static let unknown = 1000
    }
}
