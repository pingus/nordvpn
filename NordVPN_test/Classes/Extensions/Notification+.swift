//
//  Notification+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import Foundation

extension Notification.Name {
    
    static var logout: NSNotification.Name {
        return Notification.Name(Constants.logoutName)
    }
    
    private enum Constants {
        static let logoutName: String = "UserLoggedOut"
    }
}
