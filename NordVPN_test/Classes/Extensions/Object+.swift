//
//  Object+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 09.05.2021.
//

import Foundation

extension NSObject {
    static var className: String {
        return "\(Self.self)"
    }
}
