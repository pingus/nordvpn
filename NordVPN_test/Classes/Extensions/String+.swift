//
//  String+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 09.05.2021.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: "Localizable", bundle: Bundle.main, value: "", comment: "")
    }
}
