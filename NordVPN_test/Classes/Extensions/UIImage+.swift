//
//  UIImage+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 09.05.2021.
//

import UIKit

extension UIImage {

    func maskWithColor(_ color: UIColor) -> UIImage? {
        let maskLayer = CALayer()
        maskLayer.bounds = CGRect(x: .zero, y: .zero, width: size.width, height: size.height)
        maskLayer.backgroundColor = color.cgColor
        maskLayer.doMask(by: self)
        let maskImage = maskLayer.toImage()
        
        return maskImage
    }

    func withAlpha(_ a: CGFloat) -> UIImage {
        return UIGraphicsImageRenderer(size: size, format: imageRendererFormat).image { (_) in
            draw(in: CGRect(origin: .zero, size: size), blendMode: .normal, alpha: a)
        }
    }
    
    func resized(to targetSize: CGSize) -> UIImage {
        var actualHeight: CGFloat = size.height
        var actualWidth: CGFloat = size.width
        let maxHeight: CGFloat = targetSize.height
        let maxWidth: CGFloat = targetSize.width
        var imgRatio: CGFloat = actualWidth / actualHeight
        let maxRatio: CGFloat = maxWidth / maxHeight
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if (imgRatio > maxRatio){
                // adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        draw(in: rect)
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage ?? UIImage()
    }
}

extension CALayer {

    func doMask(by imageMask: UIImage) {
        let maskLayer = CAShapeLayer()
        maskLayer.bounds = CGRect(x: .zero, y: .zero, width: imageMask.size.width, height: imageMask.size.height)
        bounds = maskLayer.bounds
        maskLayer.contents = imageMask.cgImage
        maskLayer.frame = CGRect(x: .zero, y: .zero, width: frame.size.width, height: frame.size.height)
        mask = maskLayer
    }

    func toImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size,
                                               isOpaque,
                                               UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

extension UIColor {

    func rgb() -> [CGFloat]? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            return [CGFloat(iRed), CGFloat(iGreen), CGFloat(iBlue), CGFloat(iAlpha)]
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}

// MARK: App Images
extension UIImage {
    
    static func icon(iconType: IconType = .username, iconState: IconState = .inactive) -> UIImage? {
        var iconName = ""
        switch iconType {
        case .username:
            iconName = Constants.usernameIconName
        case .password:
            iconName = Constants.passwordIconName
        case .iconVisible:
            iconName = Constants.visibleIconName
        case .iconInvisible:
            iconName = Constants.invisibleIconName
        case .filter:
            iconName = Constants.filterIconName
        case .logout:
            iconName = Constants.logoutIconName
        }
        if iconState == .active {
           return UIImage(named: iconName)
        }
        return UIImage(named: iconName)?.maskWithColor(.gray)
    }
}

// MARK: Image names, types
extension UIImage {

    enum IconType {
        case username
        case password
        case iconVisible
        case iconInvisible
        case filter
        case logout
    }

    enum IconState {
        case inactive
        case active
    }

    private enum Constants {
        static let usernameIconName = "user"
        static let passwordIconName = "password"
        static let visibleIconName = "visible"
        static let invisibleIconName = "invisible"
        static let filterIconName = "filter"
        static let logoutIconName = "logout"
    }
}
