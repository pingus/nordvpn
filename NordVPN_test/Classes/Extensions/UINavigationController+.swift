//
//  UINavigationController+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 19.05.2021.
//

import UIKit

// popViewController with Completion
extension UINavigationController {
    
    func popViewController(animated: Bool, completion: @escaping () -> Void ) {
        popViewController(animated: animated)
            
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
    
    func popToRootViewController(animated: Bool, completion: @escaping () -> Void ) {
        popToRootViewController(animated: animated)
        
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}
