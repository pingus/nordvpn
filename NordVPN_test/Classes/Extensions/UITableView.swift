//
//  UITableView.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 19.05.2021.
//

import UIKit

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .colorBlue
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = .titleFont
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
