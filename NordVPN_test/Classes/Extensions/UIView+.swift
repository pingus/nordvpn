//
//  UIView+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 09.05.2021.
//

import UIKit

extension UIView {
    
    class var preferredNib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
}

public extension UIView {
    // MARK: - UIView+Extensions
    
    func addSubviewWithConstraints(_ subview:UIView, offset:Bool = true) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        let views = [
            "view" : subview
        ]
        addSubview(subview)
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: offset ? "H:|[view]|" : "H:|[subview]|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views)
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: offset ? "V:|[view]|" : "V:|[subview]|", options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views))
        NSLayoutConstraint.activate(constraints)
    }
    
    internal class func xibName() -> String {
         return String(describing: self)
     }
    
    // MARK: - Private
    func prepareView() {
        let nameForXib = Self.xibName()
        let nibs = Bundle.main.loadNibNamed(nameForXib, owner: self, options: nil)
        if let view = nibs?.first as? UIView {
            view.backgroundColor = UIColor.clear
            self.frame = view.frame
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = true
            addSubviewWithConstraints(view, offset: true)
        }
    }
    
    func removeConstraints() {
        let constraints = self.superview?.constraints.filter{
            $0.firstItem as? UIView == self || $0.secondItem as? UIView == self
        } ?? []
        self.superview?.removeConstraints(constraints)
        self.removeConstraints(self.constraints)
    }
    
}

extension UIView {
    func anchorAllEdgesToSuperview() {
        self.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 9.0, *) {
            addSuperviewConstraint(constraint: topAnchor.constraint(equalTo: (superview?.topAnchor)!))
            addSuperviewConstraint(constraint: leftAnchor.constraint(equalTo: (superview?.leftAnchor)!))
            addSuperviewConstraint(constraint: bottomAnchor.constraint(equalTo: (superview?.bottomAnchor)!))
            addSuperviewConstraint(constraint: rightAnchor.constraint(equalTo: (superview?.rightAnchor)!))
        }
        else {
            for attribute : NSLayoutConstraint.Attribute in [.left, .top, .right, .bottom] {
                anchorToSuperview(attribute: attribute)
            }
        }
    }
    
    func anchorToSuperview(attribute: NSLayoutConstraint.Attribute) {
        addSuperviewConstraint(constraint: NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: superview, attribute: attribute, multiplier: 1.0, constant: 0.0))
    }
    
    func addSuperviewConstraint(constraint: NSLayoutConstraint) {
        superview?.addConstraint(constraint)
    }
    
    public func removeAllConstraints() {
           var _superview = self.superview

           while let superview = _superview {
               for constraint in superview.constraints {

                   if let first = constraint.firstItem as? UIView, first == self {
                       superview.removeConstraint(constraint)
                   }

                   if let second = constraint.secondItem as? UIView, second == self {
                       superview.removeConstraint(constraint)
                   }
               }

               _superview = superview.superview
           }

           self.removeConstraints(self.constraints)
           self.translatesAutoresizingMaskIntoConstraints = true
       }
}
