//
//  UIViewController+.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 12.05.2021.
//

import UIKit

extension UIViewController {
        
    func showError(error: NSError?, completion: ((UIAlertAction) -> Void)? = nil) {
        guard let error = error else { return }
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: "verisication_failed".localized, message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: completion))
            self?.present(alert, animated: true)
        }
    }
            
}
