//
//  FileLogger.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 12.05.2021.
//

import Foundation

class FileLogger: TextOutputStream {
    
    private enum Constants {
        static let numberOfLinesInLogFile = 15
        static let logFileName = "TestLogger.txt"
        static let logFileDirectory = "/Documents/"
    }
    
    private let queue = DispatchQueue(label: "FileLogQueue", attributes: .concurrent)
    
    private static var documentDirectoryPath: String {
        get {
            return NSHomeDirectory() + Constants.logFileDirectory
        }
    }
    
    private static var logFileName: String{
        get{
            return Constants.logFileName
        }
    }
    
    private static var logFileFullPath: String{
        get{
            return documentDirectoryPath + logFileName
        }
    }
    
    static let shared: FileLogger = FileLogger()
    
    private init() { }
    
    var fileHandle: FileHandle? = {
        if !FileManager.default.fileExists(atPath: FileLogger.logFileFullPath) {
            FileManager.default.createFile(atPath: FileLogger.logFileFullPath, contents: nil, attributes: nil)
        }
        let fileHandle = FileHandle(forWritingAtPath: FileLogger.logFileFullPath)
        return fileHandle
    }()
    
    func write(_ string: String) {
        queue.async(flags: .barrier) { [weak self] in
            guard let self = self else { return }
            if let contents = try? String(contentsOfFile: FileLogger.logFileFullPath) {
                var lines = contents.split(separator:"\n")
                if(lines.count >= Constants.numberOfLinesInLogFile) {
                    lines.removeFirst()
                    let joinedStrings = lines.joined(separator: "\n")
                    do {
                        try joinedStrings.write(toFile: FileLogger.logFileFullPath, atomically: false, encoding: .utf8)
                    } catch let error {
                        print("Error on writing strings to file: \(error)")
                    }
                }
            }
            
            self.fileHandle?.seekToEndOfFile()
            if let dataToWrite = string.data(using: String.Encoding.utf8) {
                self.fileHandle?.write(dataToWrite)
            }
        }
    }
    
    var consiliumLogFileData: Data? {
        if(FileManager.default.fileExists(atPath: FileLogger.logFileFullPath)) {
            return try? NSData(contentsOfFile: FileLogger.logFileFullPath) as Data
        } else {
            return nil
        }
    }
}
