//
//  LoggerConfiguration.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 12.05.2021.
//

import Foundation

protocol LoggerConfiguration {
    func allowToPrint() -> Bool
    func allowToLogWrite() -> Bool
    func addTimeStamp() -> Bool
    func addFileName() -> Bool
    func addFunctionName() -> Bool
    func addLineNumber() -> Bool
}

extension LoggerConfiguration {
    func addTimeStamp() -> Bool {return true}
    func addFileName() -> Bool {return false}
    func addFunctionName() -> Bool {return true}
    func addLineNumber() -> Bool {return true}
}

public enum Logger: LoggerConfiguration {
    case DEBUG, INFO, ERROR, EXCEPTION, WARNING
    
fileprivate func symbolString() -> String {
        var messgeString = ""
        switch self {
        case .DEBUG:
            messgeString =  "\u{0001F539} "
        case .INFO:
            messgeString =  "\u{0001F538} "
        case .ERROR:
            messgeString =  "\u{0001F6AB} "
        case .EXCEPTION:
            messgeString =  "\u{2757}\u{FE0F} "
        case .WARNING:
            messgeString =  "\u{26A0}\u{FE0F} "
        }
        var logLevelString = "\(self)"
        
        for _ in 0 ..< (10 - logLevelString.count)  {
            logLevelString.append(" ")
        }
        messgeString = messgeString + logLevelString + "➯ "
        return messgeString
    }
}

public func print(_ message: Any..., logLevel: Logger, _ callingFunctionName: String = #function, _ lineNumber: UInt = #line, _ fileName: String = #file) {
    let messageString = message.map({"\($0)"}).joined(separator: " ")
    var fullMessageString = logLevel.symbolString()
    
    if logLevel.addTimeStamp() {
        fullMessageString = fullMessageString + Date().ISO8601String + " ⇨ "
    }
    if logLevel.addFileName() {
        let fileName = URL(fileURLWithPath: fileName).deletingPathExtension().lastPathComponent
        fullMessageString = fullMessageString + fileName + " ⇨ "
    }
    if logLevel.addFunctionName() {
        fullMessageString = fullMessageString + callingFunctionName
        if logLevel.addLineNumber() {
            fullMessageString = fullMessageString + " : \(lineNumber)" + " ⇨ "
        } else {
            fullMessageString = fullMessageString + " ⇨ "
        }
    }
    
    fullMessageString = fullMessageString + messageString
    
    if logLevel.allowToPrint() {
        print(fullMessageString)
    }
    if logLevel.allowToLogWrite() {
        var a = FileLogger.shared
        print(fullMessageString, to: &a)
    }
}

extension LoggerConfiguration {
    func allowToPrint() -> Bool {
        var allowed = false
        if let logLevel = self as? Logger {
            switch logLevel {
            case .DEBUG:
                allowed = false
            case .INFO:
                allowed = false
            case .ERROR:
                allowed = true
            case .EXCEPTION:
                allowed = true
            case .WARNING:
                allowed = false
            }
        }
        return allowed
    }
    func allowToLogWrite() -> Bool {
        var allowed = false
        if let logLevel = self as? Logger {
            switch logLevel {
            case .DEBUG:
                allowed = false
            case .INFO:
                allowed = false
            case .ERROR:
                allowed = true
            case .EXCEPTION:
                allowed = true
            case .WARNING:
                allowed = true
            }
        }
        return allowed
    }
}
