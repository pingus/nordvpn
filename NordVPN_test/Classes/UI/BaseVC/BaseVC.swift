//
//  BaseVC.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 17.05.2021.
//

import UIKit

class BaseVC: UIViewController {
    
    private var activityView: UIActivityIndicatorView?
    private var activityLabel: UILabel?
    private var backView: UIVisualEffectView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(userLoggeOut), name: .logout, object: nil)
    }
    
    @objc func userLoggeOut() {
        RealmProvider.default.writeAsync({ (realm) in
            realm.deleteAll()
        }, then: {[weak self] (result) in
            DispatchQueue.main.async {
                self?.hideActivity()
                self?.navigationController?.popToRootViewController(animated: true)
            }
        })
    }

    func showActivity(message: String? = nil) {
        DispatchQueue.main.async {
            self.addBackView()
            self.getActivityView().startAnimating()
            self.addActivityLabel(message: message)
        }
    }
    
    func hideActivity() {
        DispatchQueue.main.async {
            self.backView?.removeFromSuperview()
            self.activityView?.stopAnimating()
            self.activityLabel?.removeFromSuperview()
        }
    }
    
    private func getActivityView() -> UIActivityIndicatorView {
        if let activityView = activityView {
            return activityView
        } else {
            let activityView = UIActivityIndicatorView()
            activityView.color = .colorGray2
            activityView.hidesWhenStopped = true
            activityView.style = .medium
            activityView.center = self.view.center
            self.view.addSubview(activityView)
            self.activityView = activityView
            return activityView
        }
    }
    
    private func addActivityLabel(message: String? = nil) {
        guard let message = message, let activityView = activityView else {
            activityLabel?.removeFromSuperview()
            return
        }
        if let activityLabel = activityLabel {
            activityLabel.text = message
        } else {
            
            let activityLabel = UILabel(frame: CGRect(x: .zero, y: activityView.center.y + Constants.labelHeight * 2, width: self.view.frame.size.width, height: Constants.labelHeight))
            activityLabel.text = message
            activityLabel.font = .descriptionFont
            activityLabel.textColor = .colorGray2
            activityLabel.textAlignment = .center
            self.view.addSubview(activityLabel)
            self.activityLabel = activityLabel
        }
    }
    
    private func addBackView() {
        if let backView = backView {
            self.view.addSubview(backView)
        } else {
            let blurEffect = UIBlurEffect(style: .light)
            let backView = UIVisualEffectView(effect: blurEffect)
            backView.frame = self.view.bounds
            backView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.addSubview(backView)
            self.backView = backView
        }
    }
    
    func setAttributedTitle(title: String?) {
        guard let title = title else {
            self.navigationItem.titleView = nil
            return
        }
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: title, attributes:[
                                                    NSAttributedString.Key.foregroundColor: UIColor.black,
                                                    NSAttributedString.Key.font: UIFont.navTitleFont])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
    }
    
}

// MARK: Constants
extension BaseVC {
    
    private enum Constants {
        static let labelHeight: CGFloat = 20
        static let backAlpha: CGFloat = 0.65
        static let buttonWidth: CGFloat = 100
        static let buttonHeight: CGFloat = 40
    }
}

// MARK: NavigationButtons
extension BaseVC {
    
    func addDoneButton() {
        let button = UIButton(frame: CGRect(x: .zero, y: .zero, width: Constants.buttonWidth, height: Constants.buttonHeight))
        button.setTitle("done".localized, for: .normal)
        button.backgroundColor = .clear
        button.setTitleColor(.colorBlue, for: .normal)
        button.addTarget(self, action: #selector(doneButtonAction), for: .touchUpInside)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func doneButtonAction() {
        
    }
    
    func addFilterButton() {
        let button = UIButton(frame: CGRect(x: .zero, y: .zero, width: Constants.buttonWidth, height: Constants.buttonHeight))
        button.setTitle("filter".localized, for: .normal)
        button.backgroundColor = .clear
        button.setTitleColor(.colorBlue, for: .normal)
        button.setImage(UIImage.icon(iconType: .filter, iconState: .active), for: .normal)
        button.addTarget(self, action: #selector(filterButtonAction), for: .touchUpInside)
        if let imageFrame = button.imageView?.frame,
               let labelFrame = button.titleLabel?.frame {
            let margin = CGFloat(20.0)
            button.titleEdgeInsets = UIEdgeInsets(top: .zero, left: imageFrame.size.width - margin, bottom: .zero, right: -imageFrame.size.width + margin
            );
            button.imageEdgeInsets = UIEdgeInsets(top: .zero, left: -labelFrame.size.width + margin, bottom: .zero, right: labelFrame.size.width - margin)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }

    func addLogoutButton() {
        let button = UIButton(frame: CGRect(x: .zero, y: .zero, width: Constants.buttonWidth, height: Constants.buttonHeight))
        button.setTitle("logout".localized, for: .normal)
        button.backgroundColor = .clear
        button.setTitleColor(.colorBlue, for: .normal)
        button.setImage(UIImage.icon(iconType: .logout, iconState: .active), for: .normal)
        button.addTarget(self, action: #selector(logoutButtonAction), for: .touchUpInside)
        if let imageFrame = button.imageView?.frame,
               let labelFrame = button.titleLabel?.frame {
            let margin = CGFloat(10.0)
            button.titleEdgeInsets = UIEdgeInsets(top: .zero, left: -imageFrame.size.width - margin, bottom: .zero, right: imageFrame.size.width + margin
            );
            button.imageEdgeInsets = UIEdgeInsets(top: .zero, left: labelFrame.size.width + margin, bottom: .zero, right: -labelFrame.size.width - margin)
        }

        button.semanticContentAttribute = .forceRightToLeft
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }

    @objc func filterButtonAction() {
        
    }

    @objc func logoutButtonAction() {
        NotificationCenter.default.post(name: .logout, object: nil)
    }

}
