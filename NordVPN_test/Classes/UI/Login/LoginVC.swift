//
//  LoginVC.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 08.05.2021.
//

import UIKit

class LoginVC: BaseVC {

    @IBOutlet weak var topStack: UIStackView!
    @IBOutlet weak var loginStack: UIStackView!
    @IBOutlet weak var loginInput: InputView!
    @IBOutlet weak var passwordInput: InputView!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        // Do any additional setup after loading the view.
    }

    override func userLoggeOut() { }
    
    private func setupView() {
        passwordInput.inputType = .password
        setupLoginButton()
        setupInputFieldsActions()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loginStack.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.4) {
            self.loginStack.isHidden = false
        }
    }
    
    private func setupLoginButton() {
        loginButton.setTitle("login".localized, for: .normal)
        loginButton.setTitle("login".localized, for: .disabled)
        setLoginButtonState()
    }
    
    private func setupInputFieldsActions() {
        loginInput.textDidChange = { [weak self] (text) in
            guard let self = self else { return nil }
            self.validateInput()
            return nil
        }
        passwordInput.textDidChange = { [weak self] (text) in
            guard let self = self else { return nil }
            self.validateInput()
            return nil
        }
    }
    
    private func validateInput() {
        if loginInput.getText()?.count ?? 0 > 0 && passwordInput.getText()?.count ?? 0 > 0 {
            setLoginButtonState(enable: true)
        } else {
            setLoginButtonState(enable: false)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}

// MARK: Login button enable/disable/actions
extension LoginVC {
    
    private func setLoginButtonState(enable: Bool = false) {
        loginButton.isEnabled = enable
        loginButton.backgroundColor = enable ? UIColor(named: Constants.enableButtonColor) : .lightGray

    }
    
    @IBAction func loginAction(_ sender: Any) {
        guard let username = loginInput.getText(), let password = passwordInput.getText() else { return }
        showActivity(message: "loading_wait".localized)
        LoginRequest.loginUser(with: username, password: password) {[weak self] status, error in
            self?.hideActivity()
            if let error = error {
                self?.showError(error: error as NSError)
                return
            } else {
                status ? self?.showMainScreen() : self?.showError(error: NSError(domain: NSError.Constants.domain, code: NSError.Codes.unknown, userInfo: NSError.Constants.serverError))
            }
        }
    }
    
    private func showMainScreen() {
        UIView.animate(withDuration: 0.4) {[weak self] in
            self?.loginStack.isHidden = true
        } completion: {[weak self] state in
            self?.loginStack.removeFromSuperview()
            self?.performSegue(withIdentifier: MainScreenVC.className, sender: self)
        }
    }
    
}

extension LoginVC {
    
    private enum Constants {
        static let enableButtonColor = "buttonColorEnabled"
    }
}
