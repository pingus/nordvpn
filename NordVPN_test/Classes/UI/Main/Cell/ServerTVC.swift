//
//  ServerTVC.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 18.05.2021.
//

import UIKit

class ServerTVC: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!
    var model: ServerModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = ""
        distanceLabel.text = ""
        setupLabels()
    }
    
    func setupWith(model: ServerModel?) {
        self.model = model
        setupLabels()
    }
    
    private func setupLabels() {
        guard let model = model else { return }
        titleLabel.text = model.name
        distanceLabel.text = "\(model.distance) " + "km".localized
    }

}
