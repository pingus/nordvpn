//
//  MainScreenVC.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 08.05.2021.
//

import UIKit

class MainScreenVC: BaseVC {

    @IBOutlet private weak var tableView: UITableView!
    
    private var refreshControl = UIRefreshControl()
    private var serversItems: [ServerModel]?
    private var sortingType: SortingType = .distace {
        didSet {
            switch sortingType {
            case .distace:
                serversItems = serversItems?.sorted(by: { $0.distance < $1.distance })
            case .alphabetical:
                serversItems = serversItems?.sorted(by: { $0.name < $1.name })
            }
            self.reloadData()
        }
    }
    private var actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    
    //Override don't do anything
    override func userLoggeOut() {}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        getLocalServerList()
        getServerList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addFilterButton()
        addLogoutButton()
        self.setAttributedTitle(title: "testio".localized)
        navigationController?.navigationBar.isHidden = false
    }
    
    private func setupTableView() {
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)

        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @objc private func refresh(_ sender: AnyObject) {
        refreshControl.endRefreshing()
        getServerList()
    }
    
    private func getServerList() {
        showActivity(message: "loading_list".localized)
        ServerListRequest.getServerList { [weak self] state, error in
            guard let self = self else { return }
            self.hideActivity()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
                self?.getLocalServerList()
            }
        }
    }
    
    private func getLocalServerList() {
        guard let serversList = try? ServerModel.fetch().detached() else { return }
        serversItems?.removeAll()
        serversItems = serversList
        reloadData()
    }

    private func reloadData() {
        tableView.reloadData()
    }
}

// MARK: TableView Delegates
extension MainScreenVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: .zero, y: .zero, width: tableView.frame.width, height: Constants.headerHeight))
        headerView.backgroundColor = .headerBackgroundColor
        
        let label = UILabel()
        label.frame = CGRect.init(x: Constants.labelOffset, y: .zero, width: headerView.frame.width/2 - Constants.labelOffset/2, height: headerView.frame.height)
        label.text = "server".localized.uppercased()
        label.font = .headerTitleFont
        label.textColor = .headerColor

        let labelDistance = UILabel()
        labelDistance.frame = CGRect.init(x: headerView.frame.width/2, y: .zero, width: headerView.frame.width/2 - Constants.labelOffset/2 - Constants.labelOffset, height: headerView.frame.height)
        labelDistance.textAlignment = .right
        labelDistance.text = "distance".localized.uppercased()
        labelDistance.font = .headerTitleFont
        labelDistance.textColor = .headerColor

        headerView.addSubview(label)
        headerView.addSubview(labelDistance)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.headerHeight
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if serversItems?.count == .zero {
            self.tableView.setEmptyMessage("no_items".localized)
        } else {
            self.tableView.restore()
        }
        return serversItems?.count ?? .zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ServerTVC.className, for: indexPath)
        let serverModel = serversItems?[indexPath.row]
        (cell as! ServerTVC).setupWith(model: serverModel)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
    
}

//MARK: Override filter buttons action

extension MainScreenVC {
    override func filterButtonAction() {
        
        actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "by_distance".localized, style: .default) {[weak self] action -> Void in
            self?.addFilterButton()
            self?.sortingType = .distace
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "alphabetical".localized, style: .default) {[weak self] action -> Void in
            self?.addFilterButton()
            self?.sortingType = .alphabetical
        }

        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)

        actionSheetController.popoverPresentationController?.sourceView = self.view

        present(actionSheetController, animated: true) { [weak self] in
            self?.addFilterButton()
            self?.addDoneButton()
        }
    }
    
    override func doneButtonAction() {
        actionSheetController.dismiss(animated: true) { [weak self] in
            self?.addFilterButton()
        }
    }
}

// MARK: Types & Constants
extension MainScreenVC {
    
    private enum SortingType {
        case distace
        case alphabetical
    }
    
    private enum Constants {
        static let rowHeight: CGFloat = 44
        static let headerHeight: CGFloat = 46
        static let labelOffset: CGFloat = 16
    }
}
