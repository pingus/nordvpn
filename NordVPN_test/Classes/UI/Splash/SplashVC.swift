//
//  SplashVC.swift
//  NordVPN_test
//
//  Created by Alexandr Chernyy on 08.05.2021.
//

import UIKit

class SplashVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.navigationBar.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.checkLoginStatus()
        }
    }
    
    private func showLoginScreen() {
        self.performSegue(withIdentifier: LoginVC.className, sender: self)
    }

    
    private func showMainScreen() {
        self.performSegue(withIdentifier: MainScreenVC.className, sender: self)
    }
    
    private func checkLoginStatus() {
        if let auth = try? AuthModel.fetch(forPrimaryKey: SingletonRecordID) {
            if auth.token.count > 0 {
                showMainScreen()
            } else {
                showLoginScreen()
            }
        } else {
            showLoginScreen()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
